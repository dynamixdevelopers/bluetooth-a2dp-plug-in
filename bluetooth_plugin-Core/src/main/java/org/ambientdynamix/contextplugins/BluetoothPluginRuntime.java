/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A2PD Bluetooth plug-in that includes state and re-connection support.
 *
 * @author Darren Carlson
 */
public class BluetoothPluginRuntime extends ContextPluginRuntime {
    public static String STATE_CONTEXT_TYPE = "org.ambientdynamix.contextplugins.bluetooth.a2dp.state";
    public static String DEVICES_CONTEXT_TYPE = "org.ambientdynamix.contextplugins.bluetooth.a2dp.devices";
    private static final int VALID_CONTEXT_DURATION = 60000;
    private final String TAG = this.getClass().getSimpleName();
    private Context context;
    private int[] ALL_BT_CONNECTION_STATES = new int[]{BluetoothProfile.STATE_CONNECTED, BluetoothProfile.STATE_CONNECTING, BluetoothProfile.STATE_DISCONNECTED, BluetoothProfile.STATE_DISCONNECTING};
    private int[] CONNECTED_BT_CONNECTION_STATES = new int[]{BluetoothProfile.STATE_CONNECTED, BluetoothProfile.STATE_CONNECTING};
    private List<ContextListenerInformation> listeners = new ArrayList<ContextListenerInformation>();
    private BluetoothAdapter mBtAdapter;
    private BluetoothA2dp mA2dpService;
    private BluetoothDevice connectedDevice;

    /*
     * Notes:
     * http://stackoverflow.com/questions/13014509/toggling-a2dp-device-android
     * https://derivedcode.wordpress.com/2013/10/09/connecting-to-a-bluetooth-a2dp-device-from-android/
     * http://alvinalexander.com/java/jwarehouse/android/core/java/android/bluetooth/BluetoothA2dp.java.shtml
     */

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }

    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        if (!listeners.contains(listenerInfo))
            listeners.add(listenerInfo);
        return true;
    }

    /**
     * Called once when the ContextPluginRuntime is first initialized. The implementing subclass should acquire the
     * resources necessary to run. If initialization is unsuccessful, the plug-ins should throw an exception and release
     * any acquired resources.
     */
    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        // Set the power scheme
        this.setPowerScheme(powerScheme);
        // Store our secure context
        this.context = this.getSecuredContext();
        // Make sure we can access the bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null)
            throw new Exception("Could not access Bluetooth Adapter!");
        // Add listener support for A2DP devices
        Log.d(TAG, "Calling getProfileProxy for BluetoothProfile.A2DP");
        mBtAdapter.getProfileProxy(context, mA2dpListener, BluetoothProfile.A2DP);
    }

    /**
     * Called by the Dynamix Context Manager to start (or prepare to start) context sensing or acting operations.
     */
    @Override
    public void start() {
        for (BluetoothDevice dev : mBtAdapter.getBondedDevices()) {
            Log.d(TAG, "Found bonded device " + dev.getName() + " with address " + dev.getAddress());
        }
        // Register for bluetooth a2dp state notifications
        context.registerReceiver(mReceiver, new IntentFilter(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED));
        context.registerReceiver(mReceiver, new IntentFilter(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED));
        Log.d(TAG, "Started!");
    }

    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() {
        // Unregister bluetooth a2dp state notifications
        try {
            context.unregisterReceiver(mReceiver);
        } catch (IllegalArgumentException e) {
//            do nothing, receiver is not registered
        }
        // Clear listener list
        listeners.clear();
        Log.d(TAG, "Stopped!");
    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {
        this.stop();
        context = null;
        Log.d(TAG, "Destroyed!");
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {
        sendContextRequestError(requestId, "handleContextRequest not supported for " + contextType + " there may be support for handleConfiguredContextRequest, check developer docs for this plug-in", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
        // Access arguments (deviceId is not always needed)
        String command = config.getString("command", "none");
        String deviceId = config.getString("deviceId", "none");
        Log.d(TAG, "handleConfiguredContextRequest with command " + command);
        // Check the context type
        if (contextType.equalsIgnoreCase(DEVICES_CONTEXT_TYPE)) {
            // Handle BluetoothA2dp not-connected
            if (mA2dpService == null) {
                Log.w(TAG, "BluetoothA2dp service not connected");
                sendContextRequestError(requestId, "BluetoothA2dp service not connected", ErrorCodes.STATE_ERROR);
            } else {
                // Handle command: getAllDevices
                if (command.equalsIgnoreCase("getAllDevices")) {
                    sendContextEvent(requestId, new BluetoothA2dpDevices(getAllA2DPBluetoothDevices()));
                }
                // Handle command: getConnectedDevice
                else if (command.equalsIgnoreCase("getConnectedDevice")) {
                    if (connectedDevice != null)
                        sendContextEvent(requestId, new BluetoothA2dpDevices(connectedDevice));
                    else
                        sendContextRequestError(requestId, "No connected device", ErrorCodes.NOT_FOUND);
                }
                // Handle command: connectDevice
                else if (command.equalsIgnoreCase("connectDevice")) {
                    // Try to find device using the incoming deviceId
                    BluetoothDevice dev = getDeviceForId(deviceId);
                    if (dev != null) {
                        if (connectedDevice != null && connectedDevice.getAddress().equalsIgnoreCase(deviceId)) {
                            // Already connected
                            sendContextRequestSuccess(requestId);
                        } else {
                            try {
                                // Do connect
                                doConnect(mA2dpService, dev);
                                sendContextRequestSuccess(requestId);
                            } catch (Exception e) {
                                e.printStackTrace();
                                sendContextRequestError(requestId, "Exception during connect: " + e.toString(), ErrorCodes.INTERNAL_PLUG_IN_ERROR);
                            }
                        }
                    } else {
                        sendContextRequestError(requestId, "deviceId not found (use 'deviceId' key to specify): " + deviceId, ErrorCodes.NOT_FOUND);
                    }
                }
                // Handle command: getConnectedDevice
                else if (command.equalsIgnoreCase("discoverDevices")) {
                    if (!mBtAdapter.isDiscovering()) {
                        Log.d(TAG, "Starting to discover Bluetooth devices");
                        mBtAdapter.startDiscovery();
                    } else
                        Log.d(TAG, "Already discovering Bluetooth devices");
                }
                // Handle command not found
                else {
                    Log.w(TAG, "Command not found: " + command);
                    sendContextRequestError(requestId, "Command not found: " + command, ErrorCodes.CONFIGURATION_ERROR);
                }
            }
        } else {
            Log.w(TAG, "CONTEXT_TYPE_NOT_SUPPORTED: " + contextType);
            sendContextRequestError(requestId, "CONTEXT_TYPE_NOT_SUPPORTED: " + contextType, ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
        }
    }

    /**
     * Returns the Bluetooth device for the specified deviceId (i.e., the device's mac addess),
     * or null if no device can be found.
     */
    private BluetoothDevice getDeviceForId(String deviceId) {
        if (deviceId != null) {
            List<BluetoothDevice> bonded = getAllA2DPBluetoothDevices();
            for (BluetoothDevice dev : bonded) {
                if (dev.getAddress().equalsIgnoreCase(deviceId)) {
                    return dev;
                }
            }
        }
        // No device found for deviceId, return null
        return null;
    }

    /**
     * Returns all bonded A2DP bluetooth devices, or an empty list if nothing is bonded.
     */
    private List<BluetoothDevice> getAllA2DPBluetoothDevices() {
        List<BluetoothDevice> devices = new ArrayList<BluetoothDevice>();
        if (mA2dpService != null) {
            devices = new ArrayList<BluetoothDevice>();
            for (BluetoothDevice dev : mA2dpService.getDevicesMatchingConnectionStates(ALL_BT_CONNECTION_STATES)) {
                Log.d(TAG, "Found BluetoothA2dp Device: " + dev.getName() + ". Connected state = " + mA2dpService.getConnectionState(dev));
                devices.add(dev);
            }
            Log.d(TAG, "getAllA2DPBluetoothDevices found " + devices);

        } else
            Log.w(TAG, "mA2dpService is null");
        return devices;
    }

    /**
     * ServiceListener for the BluetoothProfile.
     */
    private BluetoothProfile.ServiceListener mA2dpListener = new BluetoothProfile.ServiceListener() {
        @Override
        public void onServiceConnected(int profile, BluetoothProfile a2dp) {
            Log.d(TAG, "BluetoothProfile.ServiceListener onServiceConnected for profile = " + a2dp);
            if (profile == BluetoothProfile.A2DP) {
                // Store the BluetoothA2dp service
                mA2dpService = (BluetoothA2dp) a2dp;
                // Broadcast the connected devices
                Log.d(TAG, "onServiceConnected for profile " + profile);
            }
        }

        @Override
        public void onServiceDisconnected(int profile) {
            Log.d(TAG, "onServiceDisconnected for profile " + profile);
            // Null out the BluetoothA2dp service
            mA2dpService = null;
            connectedDevice = null;
            sendBroadcastContextEvent(new BluetoothA2dpState());
        }
    };
    /**
     * BroadcastReceiver for hooking Bluetooth events.
     */
    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            if (action.equals(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED)) {
                int state = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, BluetoothA2dp.STATE_DISCONNECTED);
                if (state == BluetoothA2dp.STATE_CONNECTED) {
                    connectedDevice = device;
                    Log.d(TAG, "Setting connected device to: Name " + device.getName() + " with address " + device.getAddress());
                    sendBroadcastContextEvent(new BluetoothA2dpState(device, "STATE_CONNECTED"));
                } else if (state == BluetoothA2dp.STATE_DISCONNECTED) {
                    connectedDevice = null;
                    sendBroadcastContextEvent(new BluetoothA2dpState(device, "STATE_DISCONNECTED"));
                    Log.d(TAG, "STATE_DISCONNECTED: Name " + device.getName() + " with address " + device.getAddress());
                }
            } else if (action.equals(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED)) {
                int state = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, BluetoothA2dp.STATE_NOT_PLAYING);
                if (state == BluetoothA2dp.STATE_PLAYING) {
                    Log.d(TAG, "STATE_PLAYING: Name " + device.getName() + " with address " + device.getAddress());
                    sendBroadcastContextEvent(new BluetoothA2dpState(device, "STATE_PLAYING"));
                } else {
                    Log.d(TAG, "STATE_NOT_PLAYING: Name " + device.getName() + " with address " + device.getAddress());
                    sendBroadcastContextEvent(new BluetoothA2dpState(connectedDevice, "STATE_NOT_PLAYING"));
                }
            } else {
                Log.w(TAG, "Unhandled a2dp action " + action);
            }
        }
    };

    /**
     * Uses Java reflection to try and connect the host to the specified Bluetooth device.
     */
    private void doConnect(BluetoothA2dp a2dp, BluetoothDevice deviceToConnect) throws Exception {
        Log.d(TAG, "Connecting to " + deviceToConnect.getName() + " with id " + deviceToConnect);
        a2dp.getClass()
                .getMethod("connect", BluetoothDevice.class)
                .invoke(a2dp, deviceToConnect);
    }
}