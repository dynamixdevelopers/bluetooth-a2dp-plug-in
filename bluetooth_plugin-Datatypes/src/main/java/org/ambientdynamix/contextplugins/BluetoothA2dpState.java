package org.ambientdynamix.contextplugins;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;
import org.ambientdynamix.api.application.IContextInfo;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents the state of an a2dp bluetooth device.
 *
 * @author Darren Carlson
 */
public class BluetoothA2dpState implements IBluetoothA2dpState {
    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     */
    public static Parcelable.Creator<BluetoothA2dpState> CREATOR = new Parcelable.Creator<BluetoothA2dpState>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
         * previously been written by Parcelable.writeToParcel().
         */
        public BluetoothA2dpState createFromParcel(Parcel in) {
            return new BluetoothA2dpState(in);
        }

        /**
         * Create a new array of the Parcelable class.
         */
        public BluetoothA2dpState[] newArray(int size) {
            return new BluetoothA2dpState[size];
        }
    };
    // Public data
    public static String STATE_CONTEXT_TYPE = "org.ambientdynamix.contextplugins.bluetooth.a2dp.state";
    // Private data
    private boolean ad2pConnected;
    private BluetoothDevice device;
    private String state; // State of the bt device
    private int rssi=0;

    /**
     * Returns the type of the context information represented by the IContextInfo. This string must match one of the
     * supported context information type strings described by the source ContextPlugin.
     */
    @Override
    public String getContextType() {
        return STATE_CONTEXT_TYPE;
    }

    /**
     * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
     * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
     * "instanceof" compare can also be used for this purpose.
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * Returns a Set of supported string-based context representation format types or null if no representation formats
     * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
     * supported representation types.
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    /**
     * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
     * "application/json") or null if the requested format is not supported.
     */
    @Override
    public String getStringRepresentation(String format) {
        if (format.equalsIgnoreCase("text/plain")) {
            return "ad2pConnected " + ad2pConnected + ", device " + device + ",  state " + state;
        } else
            // Format not supported, so return an empty string
            return "";
    }

    /**
     * Create a BluetoothA2dpState with ad2pConnected to false. No device or state is allowed in this case.
     */
    public BluetoothA2dpState() {
        ad2pConnected = false;
    }

    /**
     * Create a BluetoothA2dpState with the incoming device and state, setting ad2pConnected to true.
     */
    public BluetoothA2dpState(BluetoothDevice device, String state) {
        this.device = device;
        this.state = state;
        ad2pConnected = true;
        //this.rssi = device.get

    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    /**
     * Returns true if the ad2p stack is connected; false otherwise.
     */
    public boolean isAd2pConnected() {
        return ad2pConnected;
    }

    /**
     * Returns the ad2p Bluetooth device associated with the state, or null if the ad2p stack is disconnected.
     */
    public BluetoothDevice getDevice() {
        return this.device;
    }

    /**
     * Returns the ad2p state, or null if the ad2p stack is disconnected.
     */
    public String getState() {
        return this.state;
    }

    /**
     * Returns the device's received signal strength (RSSI) value in dBm, or 0 if the value cannot be ascertained.
     * Note that this value will only be valid during a device discovery scan, which can be power hungry, so
     * use sparingly.
     */
    public int getRssi(){
        return rssi;
    }

    /**
     * Used by Parcelable when sending (serializing) data over IPC.
     */
    public void writeToParcel(Parcel out, int flags) {
        // Handle ad2p connected state first
        out.writeByte((byte) (ad2pConnected ? 1 : 0));
        if (ad2pConnected) {
            out.writeParcelable(device, flags);
            out.writeString(state);
            out.writeInt(rssi);
        }
    }

    /**
     * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
     */
    private BluetoothA2dpState(final Parcel in) {
        // Handle ad2p connected state first
        ad2pConnected = in.readByte() == 1;
        if (ad2pConnected) {
            this.device = in.readParcelable(null);
            this.state = in.readString();
            this.rssi = in.readInt();
        }
    }

    /**
     * Default implementation that returns 0.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }
}