/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins;

import android.bluetooth.BluetoothDevice;
import org.ambientdynamix.api.application.IContextInfo;

/**
 * Represents the state of a Bluetooth a2dp device.
 *
 * @author Darren Carlson
 */
public interface IBluetoothA2dpState extends IContextInfo {
    /**
     * Returns true if the ad2p stack is connected; false otherwise.
     *
     * @return
     */
    public boolean isAd2pConnected();

    /**
     * Returns the ad2p Bluetooth device associated with the state, or null if the ad2p stack is disconnected.
     */
    public BluetoothDevice getDevice();

    /**
     * Returns the ad2p state, or null if the ad2p stack is disconnected.
     */
    public String getState();

    /**
     * Returns the device's received signal strength (RSSI) value in dBm, or 0 if the value cannot be ascertained.
     * Note that this value will only be valid during a device discovery scan, which can be power hungry, so
     * use sparingly.
     */
    public int getRssi();
}